function ProductPageCreator(domDetails){
  this.allProducts = [];
  this.allProductsShowing = this.allProducts;
  this.sideFilters = domDetails.filter;
  this.content = domDetails.content;
  this.availableFilter = ['brand', 'color', 'soldOut'];
}
ProductPageCreator.prototype.init = function(){
  this.getJsonData();
}
ProductPageCreator.prototype.getJsonData = function(){
  var _this = this;
  $.getJSON("product.json")
    .done(function(data){
      $.each(data, function(key, value){
        _this.createProduct(value);
      });
      _this.createSideFilter();
      _this.showProducts();
    });
};

ProductPageCreator.prototype.createProduct = function(value){
  var product = new Product(value);
  this.allProducts.push(product);
}

ProductPageCreator.prototype.createSideFilter = function(){
  this.createCategoryFilter("brand");
  this.sideFilters.append("<hr>");
  this.createCategoryFilter("color");
  this.sideFilters.append("<hr>");
  this.createAvailableFilter();
  this.sideFilters.append("<hr>");
}

ProductPageCreator.prototype.createCategoryFilter = function(val){
  var uniqueValuesInCategory = [];
  this.allProducts.forEach(function(element){
    var valueToinsert = (val == "brand") ? element.brand : element.color;
    uniqueValuesInCategory.push(valueToinsert);
  });
  var uniqueValuesInCategory = Array.from(new Set(uniqueValuesInCategory)).sort();
  var _this = this;
  var list = $("<ol></ol>");
  uniqueValuesInCategory.forEach(function(element){
    var colorListItem = $("<li></li>");
    var checkbox = $("<input>");
    checkbox.attr({
      type: "checkbox",
      name: val,
      value: element,
      id: element
    });
    checkbox.on("click", $.proxy(_this.checkboxClick, _this));
    var labelColor = $("<label></label>");
    labelColor.text(element);
    labelColor.attr("for", element);
    colorListItem.append(labelColor, checkbox);
    list.append(colorListItem);
    _this.sideFilters.append(list);
  });
}


ProductPageCreator.prototype.checkboxClick = function(){
  var result = this.allProducts;
  for(var i = 0; i < this.availableFilter.length; i++){
    var selected = $("input:checked").filter("[name=" + this.availableFilter[i] + "]").map(function(){ return this.value; }).get();
    if(selected.length > 0){
      var valuesForThisFIlter = [];
      for(var j = 0; j < result.length; j++){
        if(selected.includes(result[j][this.availableFilter[i]])){
          valuesForThisFIlter.push(result[j]);
        }
      }
      if(valuesForThisFIlter.length == 0){
        result = valuesForThisFIlter;
        break;
      }else{
        result = valuesForThisFIlter;
      }
    }
  }
  this.allProductsShowing = result;
  this.showProducts();
}

ProductPageCreator.prototype.createAvailableFilter = function(){
  var _this = this;
  var availableLabel = $("<label></label>");
  availableLabel.text("Available");
  availableLabel.attr("for", "available").css("margin-left", "15px");

  var availableCheckbox = $("<input>");
  availableCheckbox.attr({
    type: "checkbox",
    name: "soldOut",
    value: "0",
    id:"available"
  });

  var availableList = $("<p></p>");
  availableList.append(availableLabel, availableCheckbox);
  availableCheckbox.on("click", $.proxy(_this.checkboxClick, _this));
  this.sideFilters.append(availableList);
};

ProductPageCreator.prototype.showProducts = function(from, to){
  this.content.empty();
  var _this = this;
  this.allProductsShowing.forEach(function(element){
      var productDiv = $("<div></div>");
      productDiv.css({
        float:"left",
        border: "2px solid black",
        width: "100px",
        height: "100px",
        margin: "20px",
        padding: "10px",
        position: "relative"
      });

      var productImg = $("<img>");
      productImg.attr({
        "src": "images/"+ element.url
      }).css({
        position: "absolute",
        width: "100px",
        height: "80px",
        top:"20px"
      });

  productDiv.append(productImg);
  _this.content.append(productDiv);
  });
}

function Product(value){
  this.brand = value.brand;
  this.color = value.color;
  this.name = value.name;
  this.soldOut = value.sold_out;
  this.url = value.url;
}

$(document).ready(function(){
  var domDetails = {
    content: $("#content"),
    filter: $("#filters")
  }
  var productPageCreator = new ProductPageCreator(domDetails);
  productPageCreator.init();
})